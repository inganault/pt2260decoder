use arrayvec::ArrayVec;
use clap::{ArgEnum, Parser};
use num_complex::Complex;
use signalbool::SignalBool;
use std::{
    collections::VecDeque,
    io::{stdin, stdout, Write},
    sync::{atomic, Arc},
    thread,
    time::Instant,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Pt2260Frame {
    pub address: [u8; 8],
    pub data: [bool; 4],
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum OokState {
    Low,
    High,
}

#[derive(Debug)]
pub struct OokDecoderConfig {
    decimation: usize,
    moving_average: u32,
    packet_length_min: u32,
    packet_length_max: u32,
    envelope_skip: u32,
    bit0_min: u32,
    bit01_cen: u32,
    bit1_max: u32,
    bitlength_min: u32,
    bitlength_max: u32,
    sync_min: u32,
}
impl OokDecoderConfig {
    pub fn new(sample_rate: f32, pulse_rate: f32) -> Result<Self, &'static str> {
        let decimation = (sample_rate / (pulse_rate * 3.)).round();
        let envelope_rate = sample_rate / decimation;
        let pulsewidth = envelope_rate / pulse_rate;
        let bitwidth = pulsewidth * 4.;

        // Tolerate > 20% error
        Ok(Self {
            decimation: decimation as usize,
            moving_average: 3, // Matched filter
            packet_length_min: (bitwidth * 20.).round() as u32,
            packet_length_max: (bitwidth * 36.).round() as u32, // exact: 32
            envelope_skip: (bitwidth.round() as u32 - 1).max(0),
            bit0_min: (pulsewidth * 0.5).floor() as u32,
            bit01_cen: (pulsewidth * 2.).round() as u32,
            bit1_max: (pulsewidth * 4.).ceil() as u32,
            bitlength_min: (pulsewidth * 3.).floor() as u32,
            bitlength_max: (pulsewidth * 5.5).ceil() as u32,
            sync_min: (bitwidth * 4.).ceil() as u32, // exact: 7.75
        })
    }
}

pub struct OokDecoder {
    config: OokDecoderConfig,
    decimation_buf: Vec<f32>,
    moving_average_buf: VecDeque<f32>,
    moving_average_val: f32,
    envelope: VecDeque<f32>,
    envelope_counter: u32,
    out_buffer: VecDeque<(Pt2260Frame, f32)>,
    rssi: f32,
}
impl OokDecoder {
    pub fn new(config: OokDecoderConfig) -> Self {
        Self {
            decimation_buf: Vec::with_capacity(config.decimation),
            moving_average_buf: VecDeque::with_capacity(config.moving_average as usize),
            moving_average_val: 0.,
            envelope: VecDeque::with_capacity(config.packet_length_max as usize),
            envelope_counter: 0,
            out_buffer: VecDeque::new(),
            rssi: f32::NAN,
            config,
        }
    }
    pub fn decode(&mut self, samples: &[Complex<f32>]) {
        let decimation = self.config.decimation;
        let merged_len = samples.len() + self.decimation_buf.len();
        let power = samples.iter().map(|x| x.norm());
        if merged_len < decimation {
            self.decimation_buf.extend(power);
        } else {
            let mut merged = self.decimation_buf.clone().into_iter().chain(power);
            self.decimation_buf.clear();
            for _ in 0..(merged_len / decimation) {
                let chunk = merged.by_ref().take(decimation);
                self.decode_envelope(chunk.sum::<f32>() / (decimation as f32));
            }
            self.decimation_buf.extend(merged);
        }
    }
    pub fn get(&mut self) -> Option<(Pt2260Frame, f32)> {
        self.out_buffer.pop_front()
    }
    pub fn is_empty(&self) -> bool {
        self.out_buffer.is_empty()
    }
    pub fn rssi(&self) -> f32 {
        self.rssi
    }

    fn decode_envelope(&mut self, sample: f32) {
        // Do it sample-by-sample
        if self.moving_average_buf.len() >= self.config.moving_average as usize {
            self.moving_average_val -= self.moving_average_buf.pop_front().unwrap();
        }
        self.moving_average_val += sample;
        self.moving_average_buf.push_back(sample);

        if self.envelope.len() >= self.config.packet_length_max as usize {
            self.envelope.pop_front();
        }
        self.envelope
            .push_back(self.moving_average_val / (self.config.moving_average as f32));

        if self.envelope_counter != self.config.envelope_skip {
            self.envelope_counter += 1;
            return; // Skip this sample
        }
        self.envelope_counter = 0;

        // TODO: optimize min, max to online with self-balanced tree
        if self.envelope.len() >= self.config.packet_length_min as usize {
            let (&env_max, &env_min) = (
                self.envelope
                    .iter()
                    .max_by(|a, b| a.partial_cmp(b).unwrap())
                    .unwrap(),
                self.envelope
                    .iter()
                    .min_by(|a, b| a.partial_cmp(b).unwrap())
                    .unwrap(),
            );
            self.rssi = env_max;
            let decision = (env_max + env_min) / 2.;
            let packet = self.envelope.iter().map(|&x| {
                if x > decision {
                    OokState::High
                } else {
                    OokState::Low
                }
            });
            if let Ok(pkt) = Self::decode_packet(&self.config, packet.rev()) {
                self.envelope.clear();
                if let Ok(frame) = Self::decode_frame(&pkt) {
                    self.out_buffer.push_back((frame, env_max));
                }
            };
        }
    }
    fn decode_packet(
        config: &OokDecoderConfig,
        packet_reversed: impl Iterator<Item = OokState>,
    ) -> Result<Vec<bool>, &'static str> {
        let mut it = packet_reversed;
        // Sync
        if it
            .by_ref()
            .take(config.sync_min as usize)
            .any(|x| x == OokState::High)
        {
            return Err("No sync");
        }
        it.by_ref()
            .take_while(|&x| x == OokState::Low)
            .for_each(drop);
        let sync_high = it.by_ref().take_while(|&x| x == OokState::High).count();
        if !(config.bit0_min..=config.bit01_cen).contains(&(sync_high as u32)) {
            return Err("Bad sync");
        }
        // Data bits
        let mut prev_sample = OokState::Low;
        let mut curbit_high = 0;
        let mut curbit_period = 1;
        let mut bits = Vec::new();
        for sample in it {
            if (prev_sample, sample) == (OokState::High, OokState::Low) {
                // Falling edge
                if curbit_period < config.bitlength_min {
                    return Err("Bad bit length");
                }
                let curbit = {
                    if curbit_high < config.bit0_min {
                        return Err("Bad bit high");
                    } else if curbit_high <= config.bit01_cen {
                        false
                    } else if curbit_high <= config.bit1_max {
                        true
                    } else {
                        return Err("Bad bit high");
                    }
                };
                bits.push(curbit);
                if bits.len() == 24 {
                    break;
                }
                // Reset stats
                curbit_high = 0;
                curbit_period = 0;
            }

            // Measure bit stats
            curbit_period += 1;
            if sample == OokState::High {
                curbit_high += 1;
            }
            if curbit_period > config.bitlength_max {
                return Err("Bad bit length");
            }
            prev_sample = sample;
        }
        // Finalize
        if bits.len() != 24 {
            return Err("Incorrect amount of bit");
        }
        bits.reverse();
        Ok(bits)
    }
    fn decode_frame(bits: &[bool]) -> Result<Pt2260Frame, &'static str> {
        let address: ArrayVec<_, 8> = bits[..16]
            .chunks_exact(2)
            .map(|x| match x {
                [false, false] => Ok(b'0'),
                [true, true] => Ok(b'1'),
                [false, true] => Ok(b'f'),
                _ => Err("Bad bits"),
            })
            .collect::<Result<_, _>>()?;
        let data: ArrayVec<bool, 4> = bits[16..]
            .chunks_exact(2)
            .rev()
            .map(|x| {
                if x[0] == x[1] {
                    Ok(x[0])
                } else {
                    Err("Bad bits")
                }
            })
            .collect::<Result<_, _>>()?;
        Ok(Pt2260Frame {
            address: address.into_inner().or(Err("Insufficient address bits"))?,
            data: data.into_inner().or(Err("Insufficient data bits"))?,
        })
    }
}

#[derive(Debug)]
pub struct OokEncoderConfig {
    bit0: u32,
    bit1: u32,
    bitlength: u32,
    synclength: u32,
}
impl OokEncoderConfig {
    pub fn new(sample_rate: f32, pulse_rate: f32) -> Result<Self, &'static str> {
        let bit0 = sample_rate / pulse_rate;
        let bitlength = bit0 * 4.;
        if (bit0.round() - bit0) / bit0.abs() > 0.2 {
            return Err("Too low sample rate.");
        }
        if (bitlength.round() - bitlength) / bitlength.abs() > 0.02 {
            return Err("Too much frequency error. Increase sample rate.");
        }
        Ok(Self {
            bit0: bit0.round() as u32,
            bit1: (bit0 * 3.).round() as u32,
            bitlength: bitlength.round() as u32,
            synclength: (bitlength * 8.).round() as u32,
        })
    }
}

pub struct OokEncoder {
    config: OokEncoderConfig,
    rle: VecDeque<(OokState, u32)>,
}
impl OokEncoder {
    pub fn new(config: OokEncoderConfig) -> Self {
        Self {
            rle: VecDeque::new(),
            config,
        }
    }
    pub fn get(&mut self, out_buffer: &mut [Complex<f32>]) -> usize {
        let mut written = 0;
        loop {
            let left = out_buffer.len() - written;
            if left == 0 {
                break;
            }
            if let Some(front) = self.rle.pop_front() {
                let (val, dup) = front;
                let mut dup = dup as usize;
                if dup > left {
                    // Spill leftover back
                    self.rle.push_front((val, (dup - left) as u32));
                    dup = left;
                }
                for i in &mut out_buffer[written..(written + dup)] {
                    // TODO: some pulse shaping filter to reduce bandwidth
                    *i = match val {
                        OokState::High => 0.99.into(),
                        OokState::Low => 0.0.into(),
                    };
                }
                written += dup as usize;
            } else {
                break;
            }
        }
        written
    }
    pub fn is_empty(&self) -> bool {
        self.rle.is_empty()
    }
    pub fn encode(&mut self, frame: &Pt2260Frame, repeat: u32) -> Result<(), &'static str> {
        let bits = Self::encode_frame(frame)?;
        // Encode packet
        for _ in 0..repeat {
            for bit in &bits {
                let high = match bit {
                    false => self.config.bit0,
                    true => self.config.bit1,
                };
                self.rle.push_back((OokState::High, high));
                self.rle
                    .push_back((OokState::Low, self.config.bitlength - high));
            }
            // Sync
            self.rle.push_back((OokState::High, self.config.bit0));
            self.rle
                .push_back((OokState::Low, self.config.synclength - self.config.bit0));
        }
        Ok(())
    }
    fn encode_frame(frame: &Pt2260Frame) -> Result<Vec<bool>, &'static str> {
        let mut out = Vec::with_capacity(24);
        for addr in frame.address {
            out.extend_from_slice(match addr {
                b'0' => &[false, false],
                b'1' => &[true, true],
                b'f' => &[false, true],
                _ => return Err("Bad bits"),
            });
        }
        for &d in frame.data.iter().rev() {
            out.extend_from_slice(&[d, d]);
        }
        Ok(out)
    }
}

fn format_frame(frame: &Pt2260Frame) -> String {
    let btn_name: String = match frame.data {
        [true, false, false, false] => "BTN_Lock".into(),
        [false, false, true, false] => "BTN_Open".into(),
        [false, false, false, true] => "BTN_Light".into(),
        _ => frame
            .data
            .iter()
            .map(|x| if *x { '1' } else { '0' })
            .collect(),
    };
    format!("<{}:{}>", String::from_utf8_lossy(&frame.address), btn_name)
}
fn rx_repl(
    dev: soapysdr::Device,
    channel: usize,
    mut decoder: OokDecoder,
    sb: Arc<SignalBool>,
    stop: Arc<atomic::AtomicBool>,
) -> Result<(), String> {
    let mut stream = dev.rx_stream::<Complex<f32>>(&[channel]).unwrap();
    let mut buf = vec![Complex::new(0., 0.); stream.mtu().unwrap()];
    let enable_agc = true;

    stream
        .activate(None)
        .or(Err("failed to activate stream".to_owned()))?;

    println!("Decoder: {:?}", decoder.config);

    println!("Start reading samples, chunk=[{}]", buf.len());
    let gain_range = dev.gain_range(soapysdr::Direction::Rx, channel).unwrap();
    let mut cur_gain = dev.gain(soapysdr::Direction::Rx, channel).unwrap();
    let init_gain = cur_gain;
    let mut gain_timer = Instant::now();
    while !sb.caught() && !stop.load(atomic::Ordering::Relaxed) {
        match stream.read(&[&mut buf], 1_000_000) {
            Ok(len) => {
                decoder.decode(&buf[..len]);
                if let Some((frame, pwr)) = decoder.get() {
                    let rssi = pwr.log10() * 20. - (cur_gain as f32);
                    println!("{} @ RSSI = {:.1} dB", format_frame(&frame), rssi);
                }
                if enable_agc && gain_timer.elapsed().as_micros() > 50_000 {
                    // AGC @ 20 Hz
                    let rssi = decoder.rssi();
                    // 6dB Hysteresis
                    if rssi <= 0.025 {
                        // TooLow
                        if cur_gain <= gain_range.maximum - 1. && cur_gain < init_gain {
                            cur_gain += 1.;
                            dev.set_gain(soapysdr::Direction::Rx, channel, cur_gain)
                                .or(Err("cannot adjust gain".to_owned()))?;
                            println!("Set gain to {:.1} dB", cur_gain);
                        }
                    } else if rssi >= 0.1 {
                        // TooHigh
                        if cur_gain >= gain_range.minimum + 1. {
                            cur_gain -= 1.;
                            dev.set_gain(soapysdr::Direction::Rx, channel, cur_gain)
                                .or(Err("cannot adjust gain".to_owned()))?;
                            println!("Set gain to {:.1} dB", cur_gain);
                        }
                    }
                    gain_timer = Instant::now();
                }
            }
            Err(e) => {
                println!("read failed {:?}", e);
            }
        }
    }
    stop.store(true, atomic::Ordering::Relaxed);
    println!("\nTerminating rx");
    stream
        .deactivate(None)
        .or(Err("failed to deactivate".to_owned()))?;
    Ok(())
}
fn tx_repl(
    dev: soapysdr::Device,
    channel: usize,
    mut encoder: OokEncoder,
    sb: Arc<SignalBool>,
    stop: Arc<atomic::AtomicBool>,
) -> Result<(), String> {
    let mut stream = dev.tx_stream::<Complex<f32>>(&[channel]).unwrap();
    let mut buf = vec![Complex::new(0., 0.); stream.mtu().unwrap()];

    stream
        .activate(None)
        .or(Err("failed to activate stream".to_owned()))?;

    println!("Encoder: {:?}", encoder.config);

    println!("Start sending samples, chunk=[{}]", buf.len());
    let mut input = String::new();
    let mut frame = Pt2260Frame {
        address: *b"00000000",
        data: [false; 4],
    };
    let mut repeat = 3;

    let set_addr = |addr: &str, frame: &mut Pt2260Frame| {
        if addr.len() == 8 && addr.chars().all(|c| "01f".contains(c)) {
            frame.address.clone_from_slice(addr.as_bytes());
            println!("address = {}", String::from_utf8_lossy(&frame.address));
        } else {
            println!("invalid address");
        }
    };
    let set_data = |data: &str, frame: &mut Pt2260Frame| -> bool {
        if data.len() == 4 && data.chars().all(|c| c == '0' || c == '1') {
            data.chars()
                .map(|c| c == '1')
                .enumerate()
                .for_each(|(i, val)| {
                    frame.data[i] = val;
                });
        } else {
            let data = match data {
                "lock" => &[true, false, false, false],
                "open" => &[false, false, true, false],
                "light" => &[false, false, false, true],
                _ => {
                    println!("invalid data");
                    return false;
                }
            };
            frame.data.clone_from_slice(data);
        }
        println!(
            "data = {}",
            frame
                .data
                .iter()
                .map(|&x| if x { '1' } else { '0' })
                .collect::<String>()
        );
        true
    };
    let set_repeat = |rstr: &str, repeat: &mut u32| match rstr.parse() {
        Ok(rep) => {
            *repeat = rep;
            println!("repeat = {}", repeat);
        }
        Err(_) => {
            println!("invalid int")
        }
    };
    let mut send = |frame: &Pt2260Frame, repeat| -> Result<(), String> {
        encoder.encode(&frame, repeat).map_err(|x| x.to_owned())?;
        while !sb.caught() && !stop.load(atomic::Ordering::Relaxed) {
            let len = encoder.get(&mut buf);
            let end_of_packet = encoder.is_empty();
            match stream.write(&[&buf[..len]], None, false, 1_000_000) {
                // End burst = true, will end stream
                Err(_) => println!("write failed"),
                Ok(written) if written != len => {
                    println!("write failed dfferent size {} instead of {}", written, len)
                }
                _ => {}
            }
            if end_of_packet {
                break;
            }
        }
        Ok(())
    };
    loop {
        print!(">>> ");
        let _ = stdout().flush();
        input.clear();
        if sb.caught()
            || stop.load(atomic::Ordering::Relaxed)
            || stdin().read_line(&mut input).is_err()
        {
            break;
        }
        input = input.trim().to_lowercase();
        let input_s: Vec<&str> = input.split_whitespace().collect();
        match input_s.as_slice() {
            [] => {}
            ["exit" | "q"] => {
                break;
            }
            ["a" | "addr", addr] => {
                set_addr(*addr, &mut frame);
            }
            ["d" | "data", data] => {
                set_data(*data, &mut frame);
            }
            ["r" | "repeat", rstr] => {
                set_repeat(*rstr, &mut repeat);
            }
            ["s" | "send"] => {
                send(&frame, repeat)?;
            }
            ["s" | "send", data] => {
                if set_data(*data, &mut frame) {
                    send(&frame, repeat)?;
                }
            }
            _ => {
                println!(concat!(
                    "unknown command\n",
                    "usage:\n",
                    "q - quit\n",
                    "a|addr <address> - set address\n",
                    "d|data <data> - set data\n",
                    "r|repeat <repeat> - set repeat\n",
                    "s|send - send\n"
                ));
            }
        }
    }
    stop.store(true, atomic::Ordering::Relaxed);
    println!("\nTerminating tx");
    stream
        .deactivate(None)
        .or(Err("failed to deactivate".to_owned()))?;
    Ok(())
}

#[derive(Parser, Debug)]
#[clap(about, long_about = None)]
struct Args {
    #[clap(arg_enum)]
    direction: DirectionArg,

    /// device filter to pass to SoapySDR
    #[clap(short, long)]
    device: Option<String>,
}

#[derive(ArgEnum, Debug, Clone)]
#[clap(rename_all = "kebab_case")]
enum DirectionArg {
    Rx,
    Tx,
    Trx,
}

fn main() -> Result<(), String> {
    let args = Args::parse();

    let mut directions = Vec::new();
    match args.direction {
        DirectionArg::Rx => directions.push(soapysdr::Direction::Rx),
        DirectionArg::Tx => directions.push(soapysdr::Direction::Tx),
        DirectionArg::Trx => {
            directions.extend_from_slice(&[soapysdr::Direction::Tx, soapysdr::Direction::Rx])
        }
    };

    let devs = soapysdr::enumerate({
        if let Some(device) = args.device {
            device.as_str().into()
        } else {
            soapysdr::Args::new()
        }
    })
    .expect("Error listing devices");

    let dev_args = match devs.len() {
        0 => {
            eprintln!("No matching devices found");
            return Err("No matching devices found".into());
        }
        1 => devs.into_iter().next().unwrap(),
        n => {
            eprintln!("{} devices found.", n);
            for dev in devs {
                eprintln!("\t -d '{}'", dev);
            }
            return Err("Too many device found try specify driver with -d".into());
        }
    };

    let dev = soapysdr::Device::new(dev_args).expect("Error opening device");

    let channel = 0;

    let freq = 434e6; // 433.92 MHz
    let mut rate = 0.5e6;
    let pulserate = 3100.;

    let sb = Arc::new(
        signalbool::SignalBool::new(&[signalbool::Signal::SIGINT], signalbool::Flag::Restart)
            .map_err(|x| x.to_string())?,
    );
    let stop = Arc::new(atomic::AtomicBool::new(false));

    // Set stream config
    for &direction in &directions {
        dev.set_frequency(direction, channel, freq, ())
            .or(Err("Failed to set frequency".to_owned()))?;

        // Find suitable fs
        let mut fs_ranges = dev
            .get_sample_rate_range(direction, channel)
            .or(Err("failed to get sample rate range".to_owned()))?;
        fs_ranges.sort_by(|a, b| a.minimum.partial_cmp(&b.minimum).unwrap());
        for fs_range in fs_ranges {
            if rate <= fs_range.maximum {
                if rate < fs_range.minimum {
                    rate = fs_range.minimum;
                }
                break;
            }
        }
        dev.set_sample_rate(direction, channel, rate)
            .or(Err("failed to set sample rate".to_owned()))?;
        rate = dev
            .sample_rate(direction, channel)
            .or(Err("failed to get sample rate".to_owned()))?;
        println!("Using {:.0} Hz sampling rate", rate);

        match direction {
            soapysdr::Direction::Rx => {
                let gain_range = dev.gain_range(direction, channel).unwrap();
                let gain_mode = dev.has_gain_mode(direction, channel).unwrap();
                println!("Rx has AGC: {}", gain_mode);
                println!("Rx Gain range: {:?}", gain_range);

                // dev.set_gain(direction, channel, 0.) // Gain = 0 calibration fail, 10 ok
                //     .or(Err("failed to set gain".to_owned()))?
            }
            soapysdr::Direction::Tx => {
                let gain_range = dev.gain_range(direction, channel).unwrap();
                println!("Tx Gain range: {:?}", gain_range);

                dev.set_gain(direction, channel, gain_range.maximum) // Gain = 0 calibration fail, 10 ok
                    .or(Err("failed to set gain".to_owned()))?
            }
        };
    }

    let mut threads: Vec<thread::JoinHandle<Result<(), String>>> = Vec::new();
    for direction in directions {
        match direction {
            soapysdr::Direction::Rx => {
                let dev = dev.clone();
                let decoder = OokDecoder::new(
                    OokDecoderConfig::new(rate as f32, pulserate).map_err(|x| x.to_owned())?,
                );
                let sb = Arc::clone(&sb);
                let stop = Arc::clone(&stop);
                threads.push(thread::spawn(move || {
                    rx_repl(dev, channel, decoder, sb, stop)?;
                    Ok(())
                }));
            }
            soapysdr::Direction::Tx => {
                let dev = dev.clone();
                let encoder = OokEncoder::new(
                    OokEncoderConfig::new(rate as f32, pulserate).map_err(|x| x.to_owned())?,
                );
                let sb = Arc::clone(&sb);
                let stop = Arc::clone(&stop);
                threads.push(thread::spawn(move || {
                    tx_repl(dev, channel, encoder, sb, stop)?;
                    Ok(())
                }));
            }
        }
    }
    // TODO: print first error from any thread
    for t in threads {
        match t.join() {
            Err(x) => println!("{:?}", x),
            Ok(Err(x)) => println!("{:?}", x),
            Ok(Ok(())) => {}
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_rev() {
        let config = OokDecoderConfig {
            decimation: 110,
            moving_average: 5,
            packet_length_min: 150,
            packet_length_max: 383,
            bit0_min: 1,
            bit01_cen: 6,
            bit1_max: 15,
            bitlength_min: 10,
            bitlength_max: 16,
            sync_min: 48,
            envelope_skip: 3,
        };
        let packet = b"011100000000011110000000001111111111000111111111100011100000000001110000000001110000000001111111111000011100000000011100000000011110000000001111111111000111111111100011111111110001111111111000111111111100011111111110000111111111100011100000000011100000000001110000000001110000000001111000000000111000000000111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        let packet = packet.iter().map(|x| match x {
            b'0' => OokState::Low,
            b'1' => OokState::High,
            _ => panic!("bad bit"),
        });
        let answer = b"001100010001111111000000"
            .iter()
            .map(|&x| x == b'1')
            .collect();
        let ret = OokDecoder::decode_packet(&config, packet.rev());
        assert_eq!(Ok(answer), ret);
    }
}
