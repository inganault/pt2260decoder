'''
Orbit OB-308T1

Phy:
433.92 MHz
ASK @ ~3.1kHz

SC2260 (PT2260 variant)

B1 = LEFT LIGHT
B2 = MID OPEN
B3 = RIGHTLOCK

   D  R  B3 NC B2 B1
16 15 14 13 12 11 10 09
|                     |
|。                    |
01 02 03 04 05 06 07 08
S1 S2 S3 S4 S5 S6 S7 S8

Address {
	0~7: S1~8
}
Data {
	0: B3, 1: float, 2: B2, 3:B1
}

SC2260 protocol
ASK
1 pulse = 128 osc clock = ~300us
1 Symbol = 8 pulse
Symbol 0 (low)  => 1000 1000
Symbol float    => 1000 1110
Symbol 1 (high) => 1110 1110
Symbol sync     => 1000 0000 0000 0000 0000 0000 0000 0000

Frame:
Address[0~7] Data[3~0] Sync
(repeat transmission while pressing)
Address {0,f,1}
Data {0,1}
Sync {Sync}


97 pulse => 30.93, 32.13, 31.23 ms => 3136 3018 3105 Hz => AVG 3087 Hz
'''

def encode(address='001100ff', data='0001'):
	lut = {
		'0': '10001000',
		'f': '10001110',
		'1': '11101110',
		's': '10000000' + 3*'00000000',
	}
	data = data[::-1]
	frame = address + data + 's'
	frame = ''.join(lut[symbol] for symbol in frame)
	return frame

BTN_OPEN  = '0100'
BTN_LIGHT = '0001'
BTN_LOCK  = '1000'
HOME_ADDRESS = '01010011'

print(encode())